package com.azoza.web.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

public class DBConnection {
    private static Connection connection;

    public static Connection getConnection() throws Exception{
        if(connection == null){
            Class.forName("com.mysql.cj.jdbc.Driver");
          connection =  DriverManager.getConnection("jdbc:mysql://localhost:3308/web_db?useSSL=false", "root","96Password$");
        }
        return connection;
    }
}
